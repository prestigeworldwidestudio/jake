var preloader = function(game) {
	//console.log("preloading");
};

preloader.prototype = {

	preload: function() {
		this.game.load.image('button', 'assets/button.png');
		this.game.load.image('slime', 'assets/slime.png');
		this.game.load.image('shoes', 'assets/shoes.png');
		this.game.load.image('title', 'assets/title.png');
		this.game.load.image('street', 'assets/street.png');
		this.game.load.image('swag', 'assets/swag.png');
		this.game.load.image('black', 'assets/black.png');
		this.game.load.image('streetstart', 'assets/streetstart.png');
	},

	create: function() {
		this.game.state.start('MainMenu');
	}

}