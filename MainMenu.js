var mainMenu = function(game) {
	//console.log("main menu");
};

mainMenu.prototype = {

	create: function() {
		var background = this.game.add.tileSprite(0, 0, 1024, 1024, 'streetstart')
		background.scale.x = 0.6;
		background.scale.y = 0.5;
		/*var button = this.game.add.button(this.game.width/2, 
			this.game.height*(3/4), 'button', this.startGame);
		button.scale.x = 0.7;
		button.scale.y = 0.8;
		button.anchor.setTo(0.5, 0.5);
		var startText = this.game.add.text(button.x, button.y, 
			"START", {font: "26px Arial", fill: "#000000"});
		startText.anchor.setTo(0.5, 0.5);*/
		var title = this.game.add.text(this.game.width/2, this.game.height*(1/2),
			"FRESH" + '\n' + "    TIL" + '\n' + "DEATH", 
			{font: "bold 80px Arial", fill: "#349C00"});
		title.anchor.setTo(0.5, 0.5);
		var pauseWarning = this.game.add.text(this.game.width/2, this.game.height*(9.5/10), 
			"*PRESS SPACEBAR TO START/PAUSE*", {font: "16px Arial", fill: "#FFFFFF"});
		pauseWarning.anchor.setTo(0.5, 0.5);

		this.game.input.keyboard.onPressCallback = this.startGame;
	},

	startGame: function(){
		//console.log("starting game");
		if(this.game.input.keyboard.lastKey.keyCode == Phaser.Keyboard.SPACEBAR){
			this.game.state.start('TheGame');
		}
	}

}