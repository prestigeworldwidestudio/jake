var theGame = function(game){
	//console.log('started the game');
};

var game;
var shoes;
var wordsArray;
var background;
var inputBuffer;
var swagMeter;
var swagMeterHeight;
var pauseScreen;
var tutorial;
var howTo;
var pauseText;
var swagValue;
var SWAGTOTAL;
var atIntro;
var atGameOver;
var points;

theGame.prototype = {

	preload: function(){
		game = this;
	},

	create: function(){
		swagValue = SWAGTOTAL = 10;
		points = 0;
		atIntro = true;
		atGameOver = false;

		//adding background and scaling it
		background = game.add.tileSprite(0, 0, 1024, 1024, 'street');
		background.scale.y = 0.5;
		background.scale.x = 0.6;

		//adds swag meter
		swagMeter = game.add.sprite(this.game.width, 
			this.game.height, 'swag');
		swagMeter.cropEnabled = true;
		var swagText = game.add.text(swagMeter.x, swagMeter.y - swagMeter.height, 
			"SWAG", 
			{font: "20px Arial", fill: "#FFFFFF"});
		swagMeter.anchor.setTo(1, 1);
		swagText.anchor.setTo(1, 1);
		swagMeter.scale.x = 0.5;
		swagMeterHeight = swagMeter.height;

		//calls that method when keys are pressed, checks input
		game.input.keyboard.onPressCallback = this.checkKeyboardInput;

		//intialize wordsArray array
		wordsArray = [];
		inputBuffer = [];

		//starting arcade physics
		game.physics.startSystem(Phaser.Physics.ARCADE);

		//adds and scale shoes
		shoes = game.add.sprite(this.game.width/18, this.game.height/80, 'shoes');
		shoes.scale.x = 0.6;
		shoes.scale.y = 0.6;

		//adding physics body to shoes
		game.physics.arcade.enable(shoes);
		shoes.enableBody = true;
		shoes.body.height = shoes.height ;

		//creates a timer for every 2 seconds and starts it
		var timer;
		timer = game.time.create(false);
		timer.loop(1000, this.timerTick, this);
		timer.start();

		//creating slimes group and adding physics body
		slimes = game.add.group();
		slimes.enableBody = true;

		//creates words group
		words = game.add.group();

		//overlays pause screen and hides it
		pauseScreen = game.add.image(0, 0, 'black');
		pauseScreen.scale.x = 2;
		pauseScreen.scale.y = 2;
		pauseScreen.alpha = 0.7;
		pauseScreen.visible = false;
		pauseText = game.add.text(this.game.width/2, this.game.height/2, 
			"PAUSED", {font: "40px Arial", fill: "#FFFFFF"});
		pauseText.anchor.setTo(0.5, 0.5);
		pauseText.visible = false;

		game.introScreen();
	},

	update: function(){
		//shifts the background
		background.tilePosition.x-=2;

		game.keepTextAboveSlime();

		//checks collision with shoes and slimes, calls slimeHit if true
		game.physics.arcade.overlap(shoes, slimes, this.slimeHit, null, this);
	},

	keepTextAboveSlime: function(){
		if (slimes != null){
			slimes.forEach(function(slime){
				var word = words.getChildAt(slimes.getIndex(slime));
				word.x = Math.floor(slime.x + word.width/2);
				word.y = Math.floor(slime.y - word.height);
			});
		}
	},

	//method to randomly create slimes
	spawnSlimes: function(){
		//gets a random number between 0-8 for the slimes's x variable
		var x = Math.floor((Math.random() * 8) + 0);
		x*=(this.game.width/10);

		//creates slime and scales, starts moving
		var slime = slimes.create(x, 500, 'slime');
		slime.scale.x = 0.2;
		slime.scale.y = 0.2;
		slime.body.velocity.y = -100;
 
 		//creates text above slime and tweens it to follow slime
		var text = new Phaser.Text(game, x + slime.width/2, this.game.height/1.3, 
			game.generateWords(), {font: "20px Arial", fill: "#FFFFFF"}, words);
		text.x -= text.width/2;
		words.add(text);

	},

	//adds keyboard input to the buffer
	checkKeyboardInput: function(){
		//checks if a user has pressed a key yet
		if (game.input.keyboard.lastKey != null){
			//if the user pressed spacebar
			if (game.input.keyboard.lastKey.keyCode == Phaser.Keyboard.SPACEBAR){
				//if pressed during gameplay
				if (atIntro != true && atGameOver != true){
					if (this.game.paused != true){
						game.pauseGame();	
					}
					else {
						game.resumeGame();
					}
				}
				//if pressed at intro screen
				else if (atIntro == true) {
					tutorial.visible = false;
					pauseWarning.visible = false;
					this.game.paused = false;
					atIntro = false;
					pauseScreen.visible = false;
				}
				//if pressed at game over screen
				else if (atGameOver == true){
					this.game.paused = false;
					this.game.state.start('MainMenu');
				}
			}
			//if the array has been filled already
			if (inputBuffer[2] != null){
				//shifts elements down one position
				inputBuffer.shift();
				inputBuffer.push(String.fromCharCode(
					game.input.keyboard.lastKey.keyCode).toLowerCase());
			}
			//if array hasn't been filled yet
			else {
				inputBuffer.push(String.fromCharCode(
					game.input.keyboard.lastKey.keyCode).toLowerCase());
			}
			game.compareToWords();
		}
	},

	//compares input buffer to generated words-
	compareToWords: function(){
		if (wordsArray != null){
			wordsArray.forEach(function(wordText){
				if ((inputBuffer.join("").localeCompare(wordText)) == 0){
					game.clearMatched(wordsArray.indexOf(wordText), wordText);
				}
			});
		}
	},

	clearMatched: function(index, wordText){
		//console.log("matched: " + wordText);
		points++;
		wordsArray.splice(words.index, 1);
		var word = words.getChildAt(index);
		var slime = slimes.getChildAt(index);
		word.destroy();
		slime.destroy();
	},

	//called on each timer tick
	timerTick: function(){
		this.spawnSlimes();
	},

	//called when a slime hits your shoes
	slimeHit: function(shoes, slime){
		//destroys slime and text on collision
		if (slime.exists == true){
			swagValue -= 1;
			if (swagValue == 0){
				game.endGame();
			}
			swagMeter.height = (swagValue/SWAGTOTAL)*swagMeterHeight;
			var word = words.getChildAt(slimes.getIndex(slime));
			wordsArray.splice(words.getIndex(word), 1);
			word.destroy();
			slime.destroy();
		}
	},

	introScreen: function(){
		this.game.paused = true;
		pauseScreen.visible = true;
		var howTo = "You just got fresh kicks, but the " + '\n' + 
			"slimes want to get them dirty. " +'\n' + 
			"Type the words above their "+ '\n' + 
			"heads to get rid of them, " +'\n' + 
			"or else they'll mess up your " + '\n' + 
			"shoes and lower your swag.";
		tutorial = game.add.text(this.game.width/2, this.game.height/2, 
			howTo, {font: "24px Arial", fill: "#FFFFFF"});
		tutorial.anchor.setTo(0.5, 0.5);
		pauseWarning = this.game.add.text(this.game.width/2, this.game.height*(9.5/10), 
			"*PRESS SPACEBAR TO START*", {font: "16px Arial", fill: "#FFFFFF"});
		pauseWarning.anchor.setTo(0.5, 0.5);
	},

	pauseGame: function(){
		this.game.paused = true;
		pauseScreen.visible = true;
		pauseText.visible = true;
	},

	resumeGame: function(){
		this.game.paused = false;
		pauseScreen.visible = false; 
		pauseText.visible = false;
	},

	endGame: function(){
		this.game.paused = true;
		pauseScreen.visible = true;
		atGameOver = true;
		var youLost = "You lost all your swag! Game over.";
		var endText = game.add.text(this.game.width/2, this.game.height/2, 
			youLost, {font: "24px Arial", fill: "#FFFFFF"});
		endText.anchor.setTo(0.5, 0.5);
		var restartText = this.game.add.text(this.game.width/2, this.game.height*(9.5/10), 
			"*PRESS SPACEBAR TO RESTART*", {font: "16px Arial", fill: "#FFFFFF"});
		restartText.anchor.setTo(0.5, 0.5);
	},

	//generates a word and adds it to the array
	generateWords: function(){
		var word = "";
		//ABCDEFGHIJKLMNOPQRSTUVWXYZ
	    var possible = "abcdefghijklmnopqrstuvwxyz";

	    for(var i = 0; i < 3; i++){
	        word += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    wordsArray.push(word);
	    return word;
	}
}