var boot = function(game) {
	//console.log("starting boot");
};

boot.prototype = {

	create: function() {

		//centers game vertically and horizontally
		this.game.scale.pageAlignVertically = true;
		this.game.scale.pageAlignHorizontally = true;

		this.game.state.start('Preloader');
	}

}